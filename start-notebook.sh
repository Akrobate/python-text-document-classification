#!/bin/bash

DOCKER_IMAGE_NAME="text-classification"
NOTEBOOK_PORT=8888
API_PORT=8800
docker build . -t $DOCKER_IMAGE_NAME

docker run \
    --rm \
    --name $DOCKER_IMAGE_NAME-container \
    -p $NOTEBOOK_PORT:8888 \
    -p $API_PORT:8800 \
    -v `pwd`/notebook:/notebook \
    -v `pwd`/api:/api \
    -it text-classification \
    bash -c "jupyter notebook --allow-root --ip=0.0.0.0 --port=8888"