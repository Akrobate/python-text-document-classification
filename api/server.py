import flask
from flask import request, jsonify
from flask_cors import CORS
from waitress import serve

from custom_logger import log
from service import predict, status, listPredict

app = flask.Flask(__name__)
CORS(app)

app.config["DEBUG"] = True

server_port = 8800
server_host = '0.0.0.0'

@app.route('/black-box/status', methods=['GET'])
def blackBox():
    result = status()
    return jsonify(result)

@app.route('/black-box/predict-scores', methods=['POST'])
def predictScoresController():
    input_params = request.get_json()
    result = predict(input_params['data'])
    log(result)
    return jsonify(result)

@app.route('/black-box/list-predict-scores', methods=['POST'])
def listPredictScoresController():
    input_params = request.get_json()
    result = listPredict(input_params['data_list'])
    log(result)
    return jsonify(result)

log("Server loaded")
serve(
    app,
    host = server_host,
    port = server_port
)
