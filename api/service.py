from joblib import load
import psutil
import spacy
nlp = spacy.load('fr_core_news_md')

classifier = load('../notebook/data/trained_model_sgdclassifier.joblib')
tfidf_vectorizer = load('../notebook/data/trained_model_tfidf_vectorizer.joblib')


def formatPrediction(text, lema_text, score_list, class_list):
    KEY_CLASS, KEY_SCORE = 0, 1
    zipped_list = list(zip(class_list, score_list))
    zipped_list.sort(key = lambda value: value[KEY_SCORE], reverse=True)
    formated_score_list = map(
        lambda value: dict({
            'class': value[KEY_CLASS],
            'score': value[KEY_SCORE],
        }),
        zipped_list
    )
    response = {
        'score_list': list(formated_score_list),
        'text': text,
        'lema_text': lema_text,
    }
    return response


def lemma_sentence(text):
    list_result = []
    for token in nlp(text):
        accpeted_pos = ['NOUN', 'VERB', 'ADJ']
        if (token.pos_ in accpeted_pos and token.is_alpha):
            list_result.append(token.lemma_)
    return " ".join(list_result)


def predict(input_data):
    lema_text = lemma_sentence(input_data)
    data_X = tfidf_vectorizer.transform([lema_text])
    predicted_proba = classifier.predict_proba(data_X)[0].tolist()
    classes = classifier.classes_.tolist()
    result = formatPrediction(input_data, lema_text, predicted_proba, classes)
    return result


def listPredict(input_data):
    lema_text_list = map(lemma_sentence, input_data)

    data_X = tfidf_vectorizer.transform(lema_text_list)
    predicted_proba_list = classifier.predict_proba(data_X).tolist()
    classes = classifier.classes_.tolist()
    # @todo format multiple predictions
    # result = formatPrediction(input_data, lema_text, predicted_proba, classes)
    return predicted_proba_list


def status():
    cpu_percent = psutil.cpu_percent()
    memory_use = psutil.virtual_memory()[2]
    return {
        'status': 'alive',
        'cpu': cpu_percent,
        'memory_use': memory_use,
    }