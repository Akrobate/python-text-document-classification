FROM debian:buster

RUN apt update

# Install Pip and Python
RUN apt install python3 python3-pip -y
RUN pip3 install --upgrade pip

# Install dev dependencies
RUN apt install libpq-dev python-dev libxml2-dev libxslt1-dev libldap2-dev libsasl2-dev libffi-dev -y

# Install project dependencies
RUN pip install jupyterlab notebook

RUN pip install pandas 

# Install Seabord and dependencies
RUN apt install zlib1g-dev libjpeg-dev libblas-dev liblapack-dev libatlas-base-dev gfortran -y
RUN pip install seaborn 


RUN pip install scikit-learn

# Install tensor flow and dependecies
RUN apt install pkg-config libhdf5-dev -y
RUN pip install tensorflow


RUN pip install -U spacy
RUN python3 -m spacy download fr_core_news_md

RUN pip install flask
RUN pip install waitress
RUN pip install -U flask-cors

RUN pip install psutil

RUN mkdir /notebook
RUN mkdir /data
RUN mkdir /api

WORKDIR /notebook
# jupyter notebook --allow-root --ip=0.0.0.0
