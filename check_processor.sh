#!/bin/bash

HAS_AVX=$(lscpu | grep avx | wc -l)

if (( $HAS_AVX == 1))
then
    echo "Processor has AVX capabilities"
else
    echo "No AVX capabilities"
fi