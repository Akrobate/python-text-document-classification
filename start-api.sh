#!/bin/bash

DOCKER_IMAGE_NAME="text-classification"
API_PORT=8800
docker build . -t $DOCKER_IMAGE_NAME

docker run \
    --rm \
    --name $DOCKER_IMAGE_NAME-container \
    -p $API_PORT:8800 \
    -v `pwd`/notebook:/notebook \
    -v `pwd`/api:/api \
    -it text-classification \
    bash -c "cd /api && python3 server.py"
